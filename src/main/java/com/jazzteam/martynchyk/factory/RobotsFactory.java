package com.jazzteam.martynchyk.factory;

import com.jazzteam.martynchyk.entity.robots.Robot;
import com.jazzteam.martynchyk.entity.robots.implemetation.RobotAirplane;
import com.jazzteam.martynchyk.entity.robots.implemetation.RobotSolder;
import com.jazzteam.martynchyk.entity.robots.implemetation.RobotTank;
import com.jazzteam.martynchyk.entity.task.TaskType;

public class RobotsFactory {
    public static Robot CreteRobot(TaskType taskType) {
        switch (taskType) {
            case KILL_SOLDER:
                return new RobotSolder(5, 10, "Калаш");
            case KILL_HEAVY:
                return new RobotTank(20, 10, 300);
            case KILL_AIR:
                return new RobotAirplane(10, 10, 2);
            default:
                return new RobotSolder(5, 10, "Калаш");
        }
    }
}
