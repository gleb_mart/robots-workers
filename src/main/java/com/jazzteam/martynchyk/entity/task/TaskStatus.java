package com.jazzteam.martynchyk.entity.task;

public enum TaskStatus {
    CREATED,
    INPROCESS,
    DONE
}
