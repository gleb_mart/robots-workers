package com.jazzteam.martynchyk.entity.robots.implemetation;

import com.jazzteam.martynchyk.entity.robots.Robot;
import com.jazzteam.martynchyk.entity.task.Task;
import com.jazzteam.martynchyk.entity.task.TaskStatus;
import com.jazzteam.martynchyk.entity.task.TaskType;

import java.util.Date;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

public class RobotSolder extends Robot {
    private String weapon;

    public RobotSolder(double damage, float speed, String weapon) {
        super(damage, speed, new HashSet<>());
        getTaskType().add(TaskType.KILLYOURSELF);
        getTaskType().add(TaskType.KILL_SOLDER);
        this.weapon = weapon;
    }

    @Override
    public void doTask(Task task) {
        task.setStatus(TaskStatus.INPROCESS);
        System.out.println("Start task " + task.getId().toString());
        try {
            TimeUnit.SECONDS.sleep((long) (damageMultiple * getDamage()));
        } catch (InterruptedException e) {
            System.out.println("Was interrupted");
        }
        System.out.println("End task " + task.getId().toString());
        task.setStatus(TaskStatus.DONE);
        task.setEndDate(new Date());
    }
}
