package com.jazzteam.martynchyk;

import com.jazzteam.martynchyk.service.ProcessorService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RobotsApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(RobotsApplication.class, args);
        ProcessorService processorService = context.getBean(ProcessorService.class);
        processorService.execute();
    }
}
