package com.jazzteam.martynchyk.config;

import com.jazzteam.martynchyk.service.ProcessorService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.jazzteam.martynchyk")
public class ProcessorConfig {
    @Bean
    public ProcessorService processorService(){
        return new ProcessorService();
    }
}
