package com.jazzteam.martynchyk.controller;

import com.jazzteam.martynchyk.entity.robots.Robot;
import com.jazzteam.martynchyk.entity.task.Task;
import com.jazzteam.martynchyk.service.ProcessorService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "api/v1/processor")
public class ProcessorController {

    private final ProcessorService processorService;

    @Autowired
    public ProcessorController(ProcessorService processorService) {
        this.processorService = processorService;
    }

    @ApiOperation(value = "Return all Tasks")
    @GetMapping(path = "/tasks")
    @ResponseStatus(HttpStatus.OK)
    public List<Task> getAllTasks() {
        return processorService.getAllTasks();
    }

    @ApiOperation(value = "Return all Robots")
    @GetMapping(path = "/robots")
    @ResponseStatus(HttpStatus.OK)
    public List<Robot> getAllRobots() {
        return processorService.getAllRobots();
    }


}
